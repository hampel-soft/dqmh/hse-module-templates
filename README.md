# HAMPEL SOFTWARE ENGINEERING's modified templates for DQMH framework

**DQMH modules containing all the changes and addons we made to DQMH to fit our needs**

The Modules and Module Templates in this repository are created and provided by HAMPEL SOFTWARE ENGINEERING (HSE, www.hampel-soft.com) and are built for and based on the Delacor Queued Message Handler (DQMH, www.delacor.com).

## :bulb: Documentation

Detailed documentation on how the actual implementation works and how to use the HSE Module Templates is hosted at [our Dokuwiki](https://dokuwiki.hampel-soft.com/code/dqmh/hse-module-templates).

### :question: FAQ
See our [FAQ](https://dokuwiki.hampel-soft.com/code/dqmh/hse-module-templates/20_faq) for comments on updating versions amongst other things.

## More Information on DQMH

- http://sine.ni.com/nips/cds/view/p/lang/de/nid/213286
- https://forums.ni.com/t5/Reference-Design-Content/Delacor-Queued-Message-Handler-DQMH/ta-p/3536892
- http://forums.ni.com/t5/Delacor-Toolkits-Discussions/bd-p/7120


## :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.

## :rocket: Installation

Or "how to (re)use these templates"...

In order to use these modules as templates in your projects, you can open the included LabVIEW project and then save the existing modules - one by one - as templates. Here's how:

- [DQMH Online Documentation](http://delacor.com/documentation/dqmh-html/AddingaNewDQMHModulefromaCustomT.html)


### :link: Dependencies

These modules depend on 

- HSE-Libraries: https://dokuwiki.hampel-soft.com/code/open-source/hse-libraries
- HSE-Logger: https://dokuwiki.hampel-soft.com/code/open-source/hse-logger
- DQMH: http://sine.ni.com/nips/cds/view/p/lang/de/nid/213286


## :bulb: Usage

If you want to use the framework helpers (hse-application and hse-configuration), you can find more information about our file and project structure at https://dokuwiki.hampel-soft.com/code/common/project-structure.


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Joerg Hampel
* Manuel Sebald
* Alexander Elbert


## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
